package net.bbmsoft.controls.audio.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;

public class BrokenCurve extends CurveBase {

	private final ObservableList<Double> linear;
	private final ObservableList<Double> broken;
	private final Map<Double, Double> internalLinearToBroken;

	public BrokenCurve(Map<Double, Double> linearToBroken) {
		this();
		setLinearToBroken(linearToBroken);
	}

	public BrokenCurve() {
		this.linear = FXCollections.observableArrayList();
		this.broken = FXCollections.observableArrayList();
		this.internalLinearToBroken = new LinkedHashMap<>();

		this.linear.addListener((Observable o) -> this.update());
		this.broken.addListener((Observable o) -> this.update());

	}

	@Override
	protected double toUnclampedUninvertedInternalRatio(double value) {

		double linearRatio = (value - this.getMin()) / this.getRange();

		return toBrokenRatio(linearRatio);
	}

	@Override
	public double toExternalValue(double brokenRatio) {

		if (brokenRatio <= 0.0) {
			return this.getMin();
		}

		if (brokenRatio >= 1.0) {
			return this.getMax();
		}

		double linearRatio = this.toLinearRatio(brokenRatio);

		return this.getMin() + linearRatio * this.getRange();
	}

	private void update() {

		if (this.linear.size() != this.broken.size()) {
			return;
		}
		
		Map<Double, Double> map = new HashMap<>();

		for (int i = 0; i < this.linear.size(); i++) {
			map.put(this.linear.get(i), this.broken.get(i));
		}

		this.setLinearToBroken(map);
	}

	private void setLinearToBroken(Map<Double, Double> linearToBroken) {

		this.internalLinearToBroken.clear();
		
		this.internalLinearToBroken.put(0.0, 0.0);
		
		List<Double> sortedKeys = new ArrayList<>(linearToBroken.keySet());
		Collections.sort(sortedKeys);
		
		sortedKeys.forEach((k) -> {
			internalLinearToBroken.put(k, linearToBroken.get(k));
		});
		this.internalLinearToBroken.put(1.0, 1.0);
	}
	

	private double toBrokenRatio(double linearRatio) {

		Iterator<Entry<Double, Double>> it = this.internalLinearToBroken.entrySet().iterator();

		double lower, lowerValue, upper = 0.0, upperValue = 0.0;

		while (it.hasNext()) {
			Entry<Double, Double> entry = it.next();
			lower = upper;
			lowerValue = upperValue;
			upper = entry.getKey();
			upperValue = entry.getValue();
			if (upper > linearRatio) {
				return toBrokenRatioBetween(linearRatio, new Point2D(lower, lowerValue),
						new Point2D(upper, upperValue));
			}
		}

		return 1.0;
	}

	private double toLinearRatio(double brokenRation) {

		Iterator<Entry<Double, Double>> it = this.internalLinearToBroken.entrySet().iterator();

		double lower, lowerValue, upper = 0.0, upperValue = 0.0;

		while (it.hasNext()) {
			Entry<Double, Double> entry = it.next();
			lower = upper;
			lowerValue = upperValue;
			upper = entry.getKey();
			upperValue = entry.getValue();
			if (upperValue > brokenRation) {
				return toLinearRatioBetween(brokenRation, new Point2D(lower, lowerValue),
						new Point2D(upper, upperValue));
			}
		}

		return 1.0;
	}

	private double toBrokenRatioBetween(double linearRatio, Point2D lower, Point2D upper) {

		// y = m * x + t
		// m = dy/dx
		// t = y - m * x

		double m = (upper.getY() - lower.getY()) / (upper.getX() - lower.getX());
		double t = upper.getY() - m * upper.getX();

		return m * linearRatio + t;
	}

	private double toLinearRatioBetween(double brokenRatio, Point2D lower, Point2D upper) {

		// y = m * x + t
		// m = dy/dx
		// t = y - m * x
		// x = (y - t) / m

		double m = (upper.getY() - lower.getY()) / (upper.getX() - lower.getX());
		double t = upper.getY() - m * upper.getX();

		return (brokenRatio - t) / m;
	}

	public ObservableList<Double> getLinear() {
		return this.linear;
	}

	public ObservableList<Double> getBroken() {
		return this.broken;
	}

}
