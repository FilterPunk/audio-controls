package net.bbmsoft.controls.audio.meter;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import net.bbmsoft.controls.audio.utils.ScreenCurve;

public class BarMeterSkin extends SkinBase<BarMeter> {

	private final Region bar;
	private final Region peak;
	private final Region clip;
	private final Rectangle barClip;
	private final Rectangle peakClip;

	private final ScreenCurve screenCurve;

	public BarMeterSkin(BarMeter barMeter) {

		super(barMeter);

		this.bar = new Region();
		this.peak = new Region();
		this.clip = new Region();
		this.barClip = new Rectangle();
		this.peakClip = new Rectangle();

		this.screenCurve = new ScreenCurve(barMeter, barMeter.orientationProperty());

		this.bar.getStyleClass().add("bar");
		this.peak.getStyleClass().add("peak");
		this.clip.getStyleClass().add("clip");

		this.barClip.setFill(Color.WHITE);
		this.peakClip.setFill(Color.WHITE);

		this.bar.setClip(this.barClip);
		this.peak.setClip(this.peakClip);

		this.clip.visibleProperty().bind(barMeter.clippedProperty());

		this.getChildren().setAll(this.bar, this.peak, this.clip);

	}

	@Override
	protected void layoutChildren(double contentX, double contentY, double contentWidth, double contentHeight) {

		BarMeter meter = this.getSkinnable();

		Insets insets = meter.getInsets();

		double leftInset = insets.getLeft();
		double rightInset = insets.getLeft();
		double topInset = insets.getLeft();
		double bottomInset = insets.getLeft();

		double areaX = contentX + leftInset;
		double areaWidth = contentWidth - leftInset - rightInset;
		double areaY = contentY + topInset;
		double areaHeight = contentHeight - topInset - bottomInset;

		this.bar.resizeRelocate(areaX, areaY, areaWidth, areaHeight);
		this.peak.resizeRelocate(areaX, areaY, areaWidth, areaHeight);

		if (meter.getOrientation() == Orientation.VERTICAL) {
			this.layoutVertically(areaX, areaY, areaWidth, areaHeight);
		} else {
			this.layoutHorizontally(areaX, areaY, areaWidth, areaHeight);
		}
	}

	private void layoutVertically(double areaX, double areaY, double areaWidth, double areaHeight) {

		BarMeter meter = this.getSkinnable();

		double clipHeight = this.clip.prefHeight(areaWidth);
		this.clip.resizeRelocate(areaX, areaY, areaWidth, clipHeight);

		double valueY = meter.getCurve().convert(meter.getValue(), this.screenCurve);
		this.barClip.setX(areaX);
		this.barClip.setY(valueY);
		this.barClip.setWidth(areaWidth);
		this.barClip.setHeight(areaHeight - valueY);

		double peakHeight = this.peak.prefHeight(areaWidth);
		double peakY = meter.getCurve().convert(meter.getPeakValue(), this.screenCurve);
		this.peakClip.setX(areaX);
		this.peakClip.setY(peakY);
		this.peakClip.setWidth(areaWidth);
		this.peakClip.setHeight(peakHeight);
	}

	private void layoutHorizontally(double areaX, double areaY, double areaWidth, double areaHeight) {

		BarMeter meter = this.getSkinnable();

		double clipWidth = this.clip.prefWidth(areaHeight);
		this.clip.resizeRelocate(areaWidth - clipWidth, areaY, clipWidth, areaHeight);

		double valueX = meter.getCurve().convert(meter.getValue(), this.screenCurve);
		this.barClip.setX(areaX);
		this.barClip.setY(areaY);
		this.barClip.setWidth(valueX);
		this.barClip.setHeight(areaHeight);

		double peakWidth = this.peak.prefWidth(areaHeight);
		double peakX = meter.getCurve().convert(meter.getPeakValue(), this.screenCurve);
		this.peakClip.setX(peakX - peakWidth);
		this.peakClip.setY(areaY);
		this.peakClip.setWidth(peakWidth);
		this.peakClip.setHeight(areaHeight);

	}
}
