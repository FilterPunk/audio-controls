package net.bbmsoft.controls.audio.eq;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.SkinBase;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import net.bbmsoft.controls.audio.Curve;
import net.bbmsoft.controls.audio.EQ;
import net.bbmsoft.controls.audio.EQ.Band;
import net.bbmsoft.controls.audio.impl.MultiTouchHelper;
import net.bbmsoft.controls.audio.impl.ResizableCanvas;
import net.bbmsoft.controls.audio.utils.LinearCurve;
import net.bbmsoft.controls.audio.utils.LogarithmicCurve;

public class ParametricEqSkin extends SkinBase<EqBase> {

	private final ResizableCanvas canvas;

	private boolean updatePending;

	private final ListChangeListener<EQ.Band> bandListener;
	private final InvalidationListener uiUpdateListener;

	private final LinearCurve canvasXCurve;
	private final LinearCurve canvasYCurve;

	private final EventHandler<KeyEvent> keyPressListener;
	private final EventHandler<KeyEvent> keyReleaseListener;
	private final EventHandler<ScrollEvent> scrollListener;
	private final EventHandler<MouseEvent> clickListener;

	private MultiTouchHelper mth;

	private boolean shiftDown;

	private EQ.Band activeBand;

	public ParametricEqSkin(EqBase control) {

		super(control);

		this.canvas = new ResizableCanvas();

		this.bandListener = this::registerListeners;
		this.uiUpdateListener = o -> this.triggerUpdate();

		this.canvas.widthProperty().addListener(this.uiUpdateListener);
		this.canvas.heightProperty().addListener(this.uiUpdateListener);

		this.canvasXCurve = new LinearCurve(new SimpleDoubleProperty(0.5), this.canvas.widthProperty().subtract(0.5),
				new SimpleBooleanProperty(false));
		this.canvasYCurve = new LinearCurve(new SimpleDoubleProperty(0.5), this.canvas.heightProperty().subtract(0.5),
				new SimpleBooleanProperty(true));

		this.keyPressListener = this::keyPressed;
		this.keyReleaseListener = this::keyReleased;
		this.scrollListener = this::scrolled;
		this.clickListener = this::clicked;

		control.getBands().forEach(this::addBandListeners);

		this.getChildren().setAll(this.canvas);
		control.getStyleClass().add("peq");

		this.addListeners();

		Platform.runLater(this::repaint);
	}

	@Override
	public void dispose() {
		EqBase eq = this.getSkinnable();
		eq.getStyleClass().remove("peq");
		this.getChildren().remove(this.canvas);
		this.removeListeners();
		super.dispose();
	}

	private void addListeners() {

		EqBase eq = this.getSkinnable();
		eq.getBands().addListener(this.bandListener);

		this.mth = new MultiTouchHelper(eq);
		this.mth.setOnTouchPressed(this::pressed);
		this.mth.setOnTouchMoved(this::dragged);
		this.mth.setOnTouchReleased(this::released);

		eq.addEventHandler(KeyEvent.KEY_PRESSED, this.keyPressListener);
		eq.addEventHandler(KeyEvent.KEY_RELEASED, this.keyReleaseListener);
		eq.addEventHandler(ScrollEvent.SCROLL, this.scrollListener);
		eq.addEventHandler(MouseEvent.MOUSE_CLICKED, this.clickListener);
		
		eq.activeProperty().addListener(this.uiUpdateListener);
	}

	private void removeListeners() {

		EqBase eq = this.getSkinnable();

		eq.activeProperty().removeListener(this.uiUpdateListener);
		
		eq.removeEventHandler(MouseEvent.MOUSE_CLICKED, this.clickListener);
		eq.removeEventHandler(ScrollEvent.SCROLL, this.scrollListener);
		eq.removeEventHandler(KeyEvent.KEY_RELEASED, this.keyReleaseListener);
		eq.removeEventHandler(KeyEvent.KEY_PRESSED, this.keyPressListener);

		this.mth.dispose();

		eq.getBands().removeListener(this.bandListener);
		eq.getBands().forEach(this::removeBandListeners);
	}

	private void triggerUpdate() {

		if (this.updatePending) {
			return;
		}

		this.updatePending = true;

		Platform.runLater(() -> {
			this.updatePending = false;
			this.repaint();
		});
	}

	private void registerListeners(Change<? extends EQ.Band> change) {
		
		while (change.next()) {
			change.getRemoved().forEach(this::removeBandListeners);
			change.getAddedSubList().forEach(this::addBandListeners);
		}
		
		this.triggerUpdate();
	}

	private void addBandListeners(EQ.Band band) {
		band.activeProperty().addListener(this.uiUpdateListener);
		band.bandTypeProperty().addListener(this.uiUpdateListener);
		band.frequencyProperty().addListener(this.uiUpdateListener);
		band.gainProperty().addListener(this.uiUpdateListener);
		band.qProperty().addListener(this.uiUpdateListener);
		band.slopeProperty().addListener(this.uiUpdateListener);
	}

	private void removeBandListeners(EQ.Band band) {
		band.activeProperty().removeListener(this.uiUpdateListener);
		band.bandTypeProperty().removeListener(this.uiUpdateListener);
		band.frequencyProperty().removeListener(this.uiUpdateListener);
		band.gainProperty().removeListener(this.uiUpdateListener);
		band.qProperty().removeListener(this.uiUpdateListener);
		band.slopeProperty().removeListener(this.uiUpdateListener);
	}

	private void repaint() {

		EqBase eq = this.getSkinnable();

		GraphicsContext g = this.canvas.getGraphicsContext2D();

		g.clearRect(0, 0, this.canvas.getWidth(), this.canvas.getHeight());

		this.paint(eq.getBands().toArray(new EQ.Band[0]));
	}

	private void paint(EQ.Band... bands) {

		EqBase eq = this.getSkinnable();

		int width = (int) Math.ceil(this.canvas.getWidth()) + 1;
		double[][] gains = new double[bands.length][width];
		double[] sum = new double[width];

		for (int b = 0; b < bands.length; b++) {

			EQ.Band band = bands[b];

			BandCurvePlotter plotter = null;
			
			switch (band.getBandType()) {
			case BELL:
				plotter = new BellCurvePlotter();
				break;
			case HIGH_PASS:
				plotter = new HighPassCurvePlotter(band.getSlope());
				break;
			case HIGH_SHELF:
				plotter = new HighShelfCurvePlotter();
				break;
			case LOW_PASS:
				plotter = new LowPassCurvePlotter(band.getSlope());
				break;
			case LOW_SHELF:
				plotter = new LowShelfCurvePlotter();
				break;
			case BAND_PASS:
			case NOTCH:
				break;
			default:
				throw new IllegalStateException("Unknown band type: " + band.getBandType());
			}

			if(band.isActive() && plotter != null) {
				for (int i = 0; i < width; i++) {
					double frequency = this.pixelToFrequency(i);
					gains[b][i] = plotter.computeGain(frequency, band);
					sum[i] += gains[b][i];
				}
			} else {
				gains[b] = null;
			}
		}

		GraphicsContext g = this.canvas.getGraphicsContext2D();

		String rgb = eq.isActive() ? "#b5ffe3" : "#888";
		
		Color strokeColor = Color.web(rgb);
		Color fillColor = Color.web(rgb, 0.3);
		
		g.setStroke(strokeColor);
		g.setFill(fillColor);
		g.setLineWidth(2);

		double maxRadius = this.canvas.getWidth() / 24;
		double minRadius = maxRadius / 4;

		Curve qCurve = new LogarithmicCurve(eq.getMinQ(), eq.getMaxQ(), true);
		Curve radiusCurve = new LinearCurve(minRadius, maxRadius, false);

		for (Band band : bands) {
			double radius = qCurve.convert(band.getQ(), radiusCurve);
			double diameter = 2 * radius + 1;
			g.strokeOval(this.frequencyToPixel(band.getFrequency()) - 1, this.gainToPixel(band.getGain()) - 1, 2, 2);
			g.fillOval(this.frequencyToPixel(band.getFrequency()) - radius - 0.5,
					this.gainToPixel(band.getGain()) - radius - 0.5, diameter, diameter);
		}

		this.drawGraph(sum);

		g.setStroke(fillColor);
		g.setFill(Color.TRANSPARENT);
		g.setLineWidth(1);

		for (double[] bandGraph : gains) {
			this.drawGraph(bandGraph);
		}
	}

	private void drawGraph(double[] bandGraph) {
		
		if(bandGraph == null) {
			return;
		}

		int width = bandGraph.length;
		GraphicsContext g = this.canvas.getGraphicsContext2D();

		double[] xs = new double[width + 2];
		double[] ys = new double[width + 2];

		for (int i = 0; i < width; i++) {
			xs[i] = i;
			ys[i] = this.gainToPixel(bandGraph[i]);
		}

		xs[width] = width;
		xs[width + 1] = 0.0;

		double neutral = this.gainToPixel(0.0);

		ys[width] = neutral;
		ys[width + 1] = neutral;

		g.strokePolyline(xs, ys, width);
		g.fillPolygon(xs, ys, xs.length);
	}

	private double gainToPixel(double gain) {
		return this.getSkinnable().getGainCurve().convert(gain, this.canvasYCurve);
	}

	private double frequencyToPixel(double frequency) {
		return this.getSkinnable().getFrequencyCurve().convert(frequency, this.canvasXCurve);
	}

	private double pixelToFrequency(double x) {
		return this.canvasXCurve.convert(x, this.getSkinnable().getFrequencyCurve());
	}

	// private double pixelToGain(double y) {
	// return this.canvasYCurve.convert(y, this.getSkinnable().getGainCurve());
	// }

	private void pressed(MultiTouchHelper.Update update) {
		
		EqBase eq = this.getSkinnable();
		
		if(eq.isTouched()) {
			return;
		}
		
		eq.touchedProperty().set(true);
		eq.requestFocus();
		this.activeBand = this.findActiveBand(update.x, update.y);
	}

	private void dragged(MultiTouchHelper.Update update) {

		if (this.activeBand == null) {
			return;
		}

		EqBase eq = this.getSkinnable();

		if (!this.activeBand.gainProperty().isBound()) {

			double deltaRatioX = this.canvasYCurve.toInternalDeltaRatio(update.screenY - update.deltaY, update.screenY);

			if (this.shiftDown) {
				deltaRatioX /= 10;
			}

			double newGain = eq.getGainCurve().changeExternalValueBy(this.activeBand.getGain(), deltaRatioX);

			this.activeBand.setGain(newGain);
		}

		if (!this.activeBand.frequencyProperty().isBound()) {

			double deltaRatioY = this.canvasXCurve.toInternalDeltaRatio(update.screenX - update.deltaX, update.screenX);

			if (this.shiftDown) {
				deltaRatioY /= 10;
			}

			double newFrequency = eq.getFrequencyCurve().changeExternalValueBy(this.activeBand.getFrequency(),
					deltaRatioY);

			this.activeBand.setFrequency(newFrequency);
		}
	}

	private void released(MultiTouchHelper.Update update) {
		this.getSkinnable().touchedProperty().set(update.touchCount > 1);
	}

	private Band findActiveBand(double x, double y) {

		ObservableList<Band> bands = this.getSkinnable().getBands();

		if (bands.isEmpty()) {
			return null;
		}

		double shortestDistance = Double.MAX_VALUE;
		Band closest = null;

		for (Band band : bands) {
			double bandX = this.frequencyToPixel(band.getFrequency());
			double bandY = this.gainToPixel(band.getGain());
			double distance = Math.sqrt(Math.pow(bandX - x, 2) + Math.pow(bandY - y, 2));
			if (distance < shortestDistance) {
				shortestDistance = distance;
				closest = band;
			}
		}

		return closest;
	}

	private void keyPressed(KeyEvent e) {
		e.consume();

		if (KeyCode.SHIFT == e.getCode()) {
			this.shiftDown = true;
		}
	}

	private void keyReleased(KeyEvent e) {
		e.consume();

		if (KeyCode.SHIFT == e.getCode()) {
			this.shiftDown = false;
			return;
		}

		if ((e.getCode() == KeyCode.DELETE || e.getCode() == KeyCode.DOWN || e.getCode() == KeyCode.PAGE_DOWN)
				&& this.getSkinnable().isTouched()) {
			this.notch();
		}
		
		if ((e.getCode() == KeyCode.BACK_SPACE || e.getCode() == KeyCode.UP || e.getCode() == KeyCode.PAGE_UP)
				&& this.getSkinnable().isTouched()) {
			this.unnotch();
		}

	}

	private void scrolled(ScrollEvent e) {
		e.consume();

		Band activeBand = this.findActiveBand(e.getX(), e.getY());
		
		if(activeBand.qProperty().isBound()) {
			return;
		}

		double pixelDeltaY = this.shiftDown ? e.getDeltaY() / 10 : e.getDeltaY();
		double modifier = 1 - pixelDeltaY * 0.005;
		double newValue = activeBand.getQ() * modifier;
		activeBand.setQ(newValue);
	}

	private void clicked(MouseEvent e) {
		e.consume();

		EqBase eq = this.getSkinnable();

		if (e.getClickCount() == 2 && e.getButton().equals(MouseButton.PRIMARY)) {
			if(!this.activeBand.gainProperty().isBound()) {
				this.activeBand.setGain(0.0);
			}
			return;
		}

		if (e.getClickCount() == 2 && e.getButton().equals(MouseButton.SECONDARY) && eq.isTouched()) {
			this.notch();
			return;
		}
		
		if (e.getClickCount() == 1 && e.getButton().equals(MouseButton.SECONDARY) && !eq.isTouched()) {
			if(!this.activeBand.activeProperty().isBound()) {
				this.activeBand.setActive(!this.activeBand.isActive());
			}
			return;
		}
	}

	private void notch() {
		
		if(this.activeBand.gainProperty().isBound()) {
			return;
		}
		
		EqBase eq = this.getSkinnable();
		double range = eq.getMaxGain() - eq.getMinGain();
		this.activeBand.setGain(this.activeBand.getGain() - range * 0.67);
	}
	
	private void unnotch() {
		
		if(this.activeBand.gainProperty().isBound()) {
			return;
		}
		
		EqBase eq = this.getSkinnable();
		double range = eq.getMaxGain() - eq.getMinGain();
		this.activeBand.setGain(this.activeBand.getGain() + range * 0.67);
	}

}
