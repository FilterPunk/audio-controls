package net.bbmsoft.controls.audio.meter;

import java.text.DecimalFormat;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.util.StringConverter;

public class MeterLabelConverter extends StringConverter<Double> {

	private final DecimalFormat format = new DecimalFormat("0.#");

	@Override
	public String toString(Double tick) {

		if (tick <= this.negativeInifity.get()) {
			return "∞";
		}

		return this.format.format(Math.abs(tick));
	}

	@Override
	public Double fromString(String string) {

		if (string.equals("-∞") || string.equals("∞")) {
			return Double.valueOf(this.negativeInifity.get());
		}

		return Double.parseDouble(string);
	}

	private final DoubleProperty negativeInifity;

	public MeterLabelConverter() {
		this.negativeInifity = new SimpleDoubleProperty(this, "negative-infinity", -Double.MAX_VALUE);
	}

	public DoubleProperty negativeInfinityProperty() {
		return this.negativeInifity;
	}

	public double getNegativeInfinity() {
		return this.negativeInifity.get();
	}

	public void setNegativeInfinity(double value) {
		this.negativeInifity.set(value);
	}
}
