package net.bbmsoft.controls.audio.eq;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import net.bbmsoft.controls.audio.fader.TouchFader;
import net.bbmsoft.controls.audio.scale.TouchControlScale;
import net.bbmsoft.controls.audio.slider.SliderLabelConverter;

public class GraphicEqBandSkin extends SkinBase<GraphicEqBand> {

	private final TouchFader fader;
	private final TouchControlScale scale;
	private final TextField textField;
	private final Label frequencyLabel;

	private final ChangeListener<Number> frequencyListener;

	private final FrequencyLabelConverter labelConverter;

	private Runnable scaleUpdate;

	public GraphicEqBandSkin(GraphicEqBand band) {

		super(band);

		this.fader = new TouchFader();
		this.scale = new TouchControlScale();
		this.textField = new TextField();
		this.frequencyLabel = new Label();

		this.textField.setPrefColumnCount(4);
		this.textField.maxWidthProperty().bind(this.textField.prefWidthProperty());
		this.textField.setAlignment(Pos.CENTER);

		this.frequencyLabel.setAlignment(Pos.CENTER);
		this.frequencyLabel.setMaxWidth(Double.MAX_VALUE);

		this.frequencyListener = (o, ov, nv) -> this.updateFrequencyLabel(nv.doubleValue());

		this.labelConverter = new FrequencyLabelConverter();

		this.fader.setScale(this.scale);
		this.fader.setTextInput(this.textField);
		this.fader.minProperty().addListener(o -> this.updateScale());
		this.fader.maxProperty().addListener(o -> this.updateScale());
		this.fader.setTickLabelConverter(new SliderLabelConverter());

		this.scale.setMajorTickLength(45.0);
		this.scale.setMinorTickLength(37.0);
		this.scale.hPosProperty().set(HPos.LEFT);
		this.scale.alignmentProperty().set(TextAlignment.CENTER);
		this.scale.setShowTickLabels(false);
		this.scale.setMajorTickColor(Color.DARKGRAY);
		this.scale.setMinorTickColor(Color.LIGHTGRAY);

		this.addListeners(band);

		this.updateFrequencyLabel(band.getBand().getFrequency());

		this.updateScale();

		VBox container = new VBox(this.frequencyLabel, new StackPane(this.scale, this.fader), this.textField);
		container.getStyleClass().add("container");
		container.setSpacing(8);

		this.getChildren().setAll(container);
	}

	@Override
	public void dispose() {
		this.removeListeners(this.getSkinnable());
		super.dispose();
	}

	private void addListeners(GraphicEqBand band) {

		this.fader.minProperty().bind(band.getEq().minGainProperty());
		this.fader.maxProperty().bind(band.getEq().maxGainProperty());

		band.getBand().frequencyProperty().addListener(this.frequencyListener);

		this.fader.valueProperty().bindBidirectional(band.getBand().gainProperty());
	}

	private void removeListeners(GraphicEqBand band) {

		this.fader.minProperty().unbind();
		this.fader.maxProperty().unbind();

		band.getBand().frequencyProperty().removeListener(this.frequencyListener);

		this.fader.valueProperty().unbindBidirectional(band.getBand().gainProperty());
	}

	private void updateFrequencyLabel(double newFrequency) {
		this.frequencyLabel.setText(this.labelConverter.toString(newFrequency));
	}

	private void updateScale() {

		if (this.scaleUpdate == null) {
			Platform.runLater(this.scaleUpdate = this::doUpdateScale);
		}
	}

	private void doUpdateScale() {

		this.scaleUpdate = null;

		int majorTickUnit = 12;
		int minorTickUnit = 3;

		List<Double> majorTicks = new ArrayList<>();
		List<Double> minorTicks = new ArrayList<>();

		EqBase eq = this.getSkinnable().getEq();

		double minGain = eq.getMinGain();
		double maxGain = eq.getMaxGain();

		int start = (((int) Math.ceil(minGain) / majorTickUnit) * majorTickUnit);
		int end = (((int) Math.floor(maxGain) / majorTickUnit) * majorTickUnit);

		for (int i = start; i <= end; i += majorTickUnit) {
			majorTicks.add(Double.valueOf(i));
		}

		for (int i = start; i <= end; i += minorTickUnit) {
			if (i % majorTickUnit != 0) {
				minorTicks.add(Double.valueOf(i));
			}
		}

		this.scale.getMajorTicks().setAll(majorTicks);
		this.scale.getMinorTicks().setAll(minorTicks);
	}

}
