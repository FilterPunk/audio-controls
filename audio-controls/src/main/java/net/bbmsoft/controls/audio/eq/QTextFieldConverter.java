package net.bbmsoft.controls.audio.eq;

import java.text.DecimalFormat;

import javafx.util.StringConverter;

public class QTextFieldConverter extends StringConverter<Double> {
	
	private final DecimalFormat format001 = new DecimalFormat("0.000");
	private final DecimalFormat format01 = new DecimalFormat("0.00");
	private final DecimalFormat format1 = new DecimalFormat("0.0");

	@Override
	public String toString(Double object) {

		double doubleValue = object.doubleValue();

		if(doubleValue < 0.1) {
			return this.format001.format(doubleValue);
		} else if(doubleValue < 1.0) {
			return this.format01.format(doubleValue);
		} else if(doubleValue < 10.0) {
			return this.format1.format(doubleValue);
		} else {
			return String.valueOf(object.intValue());
		}
	}

	@Override
	public Double fromString(String string) {
		return Double.parseDouble(string.trim());
	}

}
