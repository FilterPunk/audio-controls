package net.bbmsoft.controls.audio.fader;

import javafx.scene.control.Skin;
import net.bbmsoft.controls.audio.Fader;
import net.bbmsoft.controls.audio.eq.GainTextFieldConverter;
import net.bbmsoft.controls.audio.impl.SingleAxisControlBase;

public class TouchFader extends SingleAxisControlBase implements Fader {

	public TouchFader() {
		super(new GainTextFieldConverter(false));
		((GainTextFieldConverter) this.getTextFieldConverter()).negativeInfinityProperty().bind(this.minProperty());
	}

	@Override
	protected Skin<?> createDefaultSkin() {
		return new TouchFaderSkin(this);
	}

	@Override
	public String getUserAgentStylesheet() {
		return this.getClass().getResource("fader.css").toExternalForm();
	}
}
