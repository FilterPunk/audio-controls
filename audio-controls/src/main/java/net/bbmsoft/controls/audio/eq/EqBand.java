package net.bbmsoft.controls.audio.eq;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import net.bbmsoft.controls.audio.EQ;

public class EqBand implements EQ.Band {

	private final DoubleProperty frequency;
	private final DoubleProperty gain;
	private final DoubleProperty q;
	private final IntegerProperty slope;
	private final ObjectProperty<Type> type;
	private final BooleanProperty active;

	public EqBand(EQ eq) {

		this.frequency = new SimpleDoubleProperty(this, "frequency", 1_000.0) {
			protected void invalidated() {
				double value = this.get();
				double max = eq.getMaxFrequency();
				double min = eq.getMinFrequency();
				if (value > max || value < min) {
					this.set(clamp(min, value, max));
				}
			};
		};

		this.gain = new SimpleDoubleProperty(this, "gain", 0.0) {
			@Override
			protected void invalidated() {
				double value = this.get();
				double max = eq.getMaxGain();
				double min = eq.getMinGain();
				if (value > max || value < min) {
					this.set(clamp(min, value, max));
				}
			}
		};

		this.q = new SimpleDoubleProperty(this, "q", 1.0) {
			@Override
			protected void invalidated() {
				double value = this.get();
				double max = eq.getMaxQ();
				double min = eq.getMinQ();
				if (value > max || value < min) {
					this.set(clamp(min, value, max));
				}
			}
		};

		this.slope = new SimpleIntegerProperty(this, "slope", 12);

		this.type = new SimpleObjectProperty<>(this, "type", Type.BELL);
		this.active = new SimpleBooleanProperty(this, "active", true);
	}

	@Override
	public DoubleProperty frequencyProperty() {
		return this.frequency;
	}

	@Override
	public double getFrequency() {
		return this.frequency.get();
	}

	@Override
	public void setFrequency(double value) {
		this.frequency.set(value);
	}

	@Override
	public DoubleProperty gainProperty() {
		return this.gain;
	}

	@Override
	public double getGain() {
		return this.gain.get();
	}

	@Override
	public void setGain(double value) {
		this.gain.set(value);
	}

	@Override
	public DoubleProperty qProperty() {
		return this.q;
	}

	@Override
	public double getQ() {
		return this.q.get();
	}

	@Override
	public void setQ(double value) {
		this.q.set(value);
	}

	@Override
	public ObjectProperty<Type> bandTypeProperty() {
		return this.type;
	}

	@Override
	public Type getBandType() {
		return this.type.get();
	}

	@Override
	public void setBandType(Type value) {
		this.type.set(value);
	}

	@Override
	public BooleanProperty activeProperty() {
		return this.active;
	}

	@Override
	public boolean isActive() {
		return this.active.get();
	}

	@Override
	public void setActive(boolean value) {
		this.active.set(value);
	}

	@Override
	public IntegerProperty slopeProperty() {
		return this.slope;
	}

	@Override
	public int getSlope() {
		return this.slope.get();
	}

	@Override
	public void setSlope(int value) {
		this.slope.set(value);
	}

	private double clamp(double min, double target, double max) {

		if (min > max) {
			throw new IllegalStateException("min is greater than max");
		}

		return Math.max(min, Math.min(target, max));
	}

}
