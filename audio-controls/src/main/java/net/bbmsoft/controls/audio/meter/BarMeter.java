package net.bbmsoft.controls.audio.meter;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.css.PseudoClass;
import javafx.event.EventHandler;
import javafx.scene.control.Labeled;
import javafx.scene.control.Skin;
import javafx.scene.input.MouseEvent;
import net.bbmsoft.controls.audio.Meter;
import net.bbmsoft.controls.audio.impl.SingleAxisWidetBase;

public class BarMeter extends SingleAxisWidetBase implements Meter {

	private static final PseudoClass PSEUDO_CLASS_CLIPPED = PseudoClass.getPseudoClass("clipped");

	private final DoubleProperty peakValue;
	private final ReadOnlyBooleanWrapper clipped;
	private final DoubleProperty fallback;
	private final DoubleProperty hold;
	private final DoubleProperty peakFallback;
	private final DoubleProperty peakHold;
	private final BooleanProperty autoFallback;
	private final EventHandler<MouseEvent> clipResetListener;

	public BarMeter() {

		this.setMax(0.0);
		this.setMin(-91.0);

		this.clipped = new ReadOnlyBooleanWrapper(this, "clipped", false) {
			@Override
			protected void invalidated() {
				Labeled label = getLabel();
				if (label != null) {
					label.pseudoClassStateChanged(PSEUDO_CLASS_CLIPPED, this.get());
				}
				pseudoClassStateChanged(PSEUDO_CLASS_CLIPPED, this.get());
			}
		};
		this.peakValue = new SimpleDoubleProperty(this, "peak-value", this.getMin()) {
			@Override
			protected void invalidated() {
				requestLayout();
				clipped.set(isClipped() || this.get() >= getMax());
			}
		};
		this.fallback = new SimpleDoubleProperty(this, "fallback", this.getMax() - this.getMin());
		this.hold = new SimpleDoubleProperty(this, "hold", 100);
		this.peakFallback = new SimpleDoubleProperty(this, "peak-fallback", this.getMax() - this.getMin());
		this.peakHold = new SimpleDoubleProperty(this, "peak-hold", 2_000);
		this.autoFallback = new SimpleBooleanProperty(this, "auto-fallback", false);
		this.clipResetListener = e -> this.clipped.set(false);

		this.getStyleClass().add("bar-meter");

		this.setValue(this.getMin());
		this.setPeakValue(this.getMin());

		this.labelProperty().addListener((o, ov, nv) -> {
			if (ov != null) {
				ov.removeEventHandler(MouseEvent.MOUSE_RELEASED, this.clipResetListener);
			}
			if (nv != null) {
				nv.addEventHandler(MouseEvent.MOUSE_RELEASED, this.clipResetListener);
			}
		});

		this.addEventHandler(MouseEvent.MOUSE_RELEASED, this.clipResetListener);
	}

	@Override
	public final DoubleProperty peakValueProperty() {
		return this.peakValue;
	}

	@Override
	public final double getPeakValue() {
		return this.peakValueProperty().get();
	}

	@Override
	public final void setPeakValue(final double peakValue) {
		this.peakValueProperty().set(peakValue);
	}

	@Override
	public final ReadOnlyBooleanProperty clippedProperty() {
		return this.clipped.getReadOnlyProperty();
	}

	@Override
	public final boolean isClipped() {
		return this.clippedProperty().get();
	}

	@Override
	public final DoubleProperty fallbackProperty() {
		return this.fallback;
	}

	@Override
	public final double getFallback() {
		return this.fallbackProperty().get();
	}

	@Override
	public final void setFallback(final double fallback) {
		this.fallbackProperty().set(fallback);
	}

	@Override
	public final DoubleProperty holdProperty() {
		return this.hold;
	}

	@Override
	public final double getHold() {
		return this.holdProperty().get();
	}

	@Override
	public final void setHold(final double hold) {
		this.holdProperty().set(hold);
	}

	@Override
	public final DoubleProperty peakFallbackProperty() {
		return this.peakFallback;
	}

	@Override
	public final double getPeakFallback() {
		return this.peakFallbackProperty().get();
	}

	@Override
	public final void setPeakFallback(final double peakFallback) {
		this.peakFallbackProperty().set(peakFallback);
	}

	@Override
	public final DoubleProperty peakHoldProperty() {
		return this.peakHold;
	}

	@Override
	public final double getPeakHold() {
		return this.peakHoldProperty().get();
	}

	@Override
	public final void setPeakHold(final double peakHold) {
		this.peakHoldProperty().set(peakHold);
	}

	@Override
	public final BooleanProperty autoFallbackProperty() {
		return this.autoFallback;
	}

	@Override
	public final boolean isAutoFallback() {
		return this.autoFallbackProperty().get();
	}

	@Override
	public final void setAutoFallback(final boolean autoFallback) {
		this.autoFallbackProperty().set(autoFallback);
	}

	@Override
	protected Skin<?> createDefaultSkin() {
		return new BarMeterSkin(this);
	}

	@Override
	public String getUserAgentStylesheet() {
		return this.getClass().getResource("meter.css").toExternalForm();
	}
}
