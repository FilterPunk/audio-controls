package net.bbmsoft.controls.audio.slider;

import javafx.scene.control.Skin;

public class PotiSlider extends SliderBase {

	@Override
	protected Skin<?> createDefaultSkin() {
		return new PotiSliderSkin(this);
	}
	
	@Override
	public String getUserAgentStylesheet() {
		return this.getClass().getResource("poti.css").toExternalForm();
	}
}
