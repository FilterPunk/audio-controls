package net.bbmsoft.controls.audio.impl;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.css.PseudoClass;
import javafx.scene.control.Control;
import net.bbmsoft.controls.audio.AudioControl;

public abstract class TouchControlBase extends Control implements AudioControl {

	private static final PseudoClass PSEUDO_CLASS_TOUCHED = PseudoClass.getPseudoClass("touched");

	private final BooleanProperty touched;

	public TouchControlBase() {
		this.touched = new SimpleBooleanProperty(this, "touched", false);
		this.touched.addListener((o, ov, nv) -> this.pseudoClassStateChanged(PSEUDO_CLASS_TOUCHED, nv));
	}

	public BooleanProperty touchedProperty() {
		return this.touched;
	}
	
	protected double clamp(double min, double target, double max) {
		
		if(min > max) {
			throw new IllegalStateException("min is greater than max");
		}
		
		return Math.max(min, Math.min(target, max));
	}
}
