package net.bbmsoft.controls.audio.example;

import javafx.beans.InvalidationListener;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.layout.GridPane;
import net.bbmsoft.controls.audio.Curve;
import net.bbmsoft.controls.audio.Scale;

public class PointEditor {

	private final Scale scale;

	public final Spinner<Double> valueSpinner;
	public final Slider brokenRatioSlider;
	public final Button deleteButton;

	public final DoubleProperty brokenRatio;

	private final ChangeListener<Curve> curveListener;
	private final InvalidationListener minMaxListener;

	public PointEditor(Scale scale, Runnable delete) {

		this.scale = scale;

		this.valueSpinner = new Spinner<>();
		this.brokenRatioSlider = new Slider();
		this.deleteButton = new Button();

		this.curveListener = (o, ov, nv) -> curveChanged(ov, nv);
		this.minMaxListener = o -> minMaxChanged();

		this.brokenRatio = this.brokenRatioSlider.valueProperty();

		this.brokenRatioSlider.setMin(0.0);
		this.brokenRatioSlider.setMax(1.0);
		this.brokenRatioSlider.setValue(0.5);
		this.brokenRatioSlider.setBlockIncrement(0.05);

		scale.curveProperty().addListener(this.curveListener);
		scale.getCurve().minProperty().addListener(this.minMaxListener);
		scale.getCurve().maxProperty().addListener(this.minMaxListener);

		this.valueSpinner.setEditable(true);

		this.brokenRatioSlider.setOrientation(Orientation.VERTICAL);

		this.deleteButton.setOnAction(e -> this.delete(scale, delete));
		this.deleteButton.setText("Delete");

		GridPane.setHalignment(this.valueSpinner, HPos.CENTER);
		GridPane.setHalignment(this.brokenRatioSlider, HPos.CENTER);
		GridPane.setHalignment(this.deleteButton, HPos.CENTER);

		this.minMaxChanged();
	}

	private void minMaxChanged() {

		double value;

		if (this.valueSpinner.getValueFactory() == null) {
			value = scale.getCurve().getRange() / 2;
		} else {
			value = this.valueSpinner.getValue();
		}

		double min = scale.getCurve().getMin();
		double max = scale.getCurve().getMax();

		DoubleSpinnerValueFactory valueFactory = new DoubleSpinnerValueFactory(min, max, clamp(min, value, max));

		double range = scale.getCurve().getRange();
		double log = Math.log10(range);
		int magnitude = (int) log;
		double step = (Math.pow(10, magnitude - 1)) / 2;

		valueFactory.setAmountToStepBy(step);

		this.valueSpinner.setValueFactory(valueFactory);
	}

	private double clamp(double min, double target, double max) {
		return Math.max(min, Math.min(target, max));
	}

	private void curveChanged(Curve ov, Curve nv) {

		if (ov != null) {
			ov.minProperty().removeListener(this.minMaxListener);
			ov.maxProperty().removeListener(this.minMaxListener);
		}

		if (nv != null) {
			nv.minProperty().addListener(this.minMaxListener);
			nv.maxProperty().addListener(this.minMaxListener);
		}
	}

	private void delete(Scale scale, Runnable delete) {

		scale.curveProperty().removeListener(this.curveListener);
		scale.getCurve().minProperty().removeListener(this.minMaxListener);
		scale.getCurve().maxProperty().removeListener(this.minMaxListener);

		delete.run();
	}
}
