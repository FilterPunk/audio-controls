package net.bbmsoft.controls.audio.example;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicReference;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import net.bbmsoft.controls.audio.Scale;
import net.bbmsoft.controls.audio.scale.TouchControlScale;
import net.bbmsoft.controls.audio.utils.BrokenCurve;

public class BrokenCurveDesigner extends Application implements Initializable {

	private final List<PointEditor> pointEditors;
	private final Map<Double, Double> pointMap;

	private Scale scale = new TouchControlScale();

	public BrokenCurveDesigner() {
		this.pointEditors = new ArrayList<>();
		this.pointMap = new HashMap<>();
	}

	@FXML
	private StackPane scaleHolder;

	@FXML
	private TextField maxTextField;

	@FXML
	private TextField minTextField;

	@FXML
	private Button addPointButton;

	@FXML
	private GridPane grid;

	@Override
	public void start(Stage primaryStage) throws Exception {

		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(this.getClass().getSimpleName() + ".fxml"));
		fxmlLoader.setController(this);
		Region root = fxmlLoader.load();
		primaryStage.setScene(new Scene(root));
		primaryStage.show();

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		this.scale.setCurve(new BrokenCurve(this.pointMap));

		this.scaleHolder.getChildren().add(this.scale.getControl());

		this.scale.topInsetProperty().set(12);
		this.scale.rightInsetProperty().set(12);
		this.scale.bottomInsetProperty().set(12);
		this.scale.leftInsetProperty().set(12);

		this.minTextField.setText(String.valueOf(this.scale.getCurve().getMin()));
		this.maxTextField.setText(String.valueOf(this.scale.getCurve().getMax()));

		this.updateScale();

	}

	@FXML
	void addPoint() {

		AtomicReference<PointEditor> edr = new AtomicReference<PointEditor>();
		PointEditor ed = new PointEditor(this.scale, () -> this.delete(edr.get()));
		edr.set(ed);

		this.pointEditors.add(ed);

		this.grid.getChildren().addAll(ed.valueSpinner, ed.brokenRatioSlider, ed.deleteButton);

		GridPane.setRowIndex(ed.valueSpinner, 0);
		GridPane.setRowIndex(ed.brokenRatioSlider, 1);
		GridPane.setRowIndex(ed.deleteButton, 2);

		ed.brokenRatio.addListener(o -> this.updateScale());
		ed.valueSpinner.valueProperty().addListener(o -> this.updateScale());

		this.updateScale();
	}

	@FXML
	void setMax() {
		double newMax = Double.parseDouble(this.maxTextField.getText());
		this.scale.getCurve().setMax(newMax);
		this.updateScale();
	}

	@FXML
	void setMin() {
		double newMin = Double.parseDouble(this.minTextField.getText());
		this.scale.getCurve().setMin(newMin);
		this.updateScale();
	}

	private void updateScale() {

		double max = Double.parseDouble(this.maxTextField.getText());
		double min = Double.parseDouble(this.minTextField.getText());

		this.pointMap.clear();

		List<Double> majorTicks = new ArrayList<>();

		majorTicks.add(min);

		for (int i = 0; i < this.pointEditors.size(); i++) {

			PointEditor ed = this.pointEditors.get(i);

			Double tick = ed.valueSpinner.getValue();
			double linearRatio = (tick - min) / (max - min);
			this.pointMap.put(linearRatio, ed.brokenRatio.get());

			GridPane.setColumnIndex(ed.valueSpinner, i + 1);
			GridPane.setColumnIndex(ed.brokenRatioSlider, i + 1);
			GridPane.setColumnIndex(ed.deleteButton, i + 1);

			majorTicks.add(tick);
		}

		majorTicks.add(max);

		this.scale.getMajorTicks().setAll(majorTicks);

		GridPane.setColumnIndex(this.addPointButton, this.pointEditors.size() + 1);

		BrokenCurve curve = new BrokenCurve(this.pointMap);
		curve.setMax(max);
		curve.setMin(min);
		this.scale.setCurve(curve);

		this.scale.getControl().requestLayout();

		System.out.println(this.pointMap);
	}

	private void delete(PointEditor pointEditor) {

		this.pointEditors.remove(pointEditor);

		this.grid.getChildren().removeAll(pointEditor.valueSpinner, pointEditor.brokenRatioSlider,
				pointEditor.deleteButton);
		this.updateScale();
		this.scale.getControl().requestLayout();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
