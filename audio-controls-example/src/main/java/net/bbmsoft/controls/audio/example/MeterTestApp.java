package net.bbmsoft.controls.audio.example;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import net.bbmsoft.controls.audio.fader.TouchFader;
import net.bbmsoft.controls.audio.meter.BarMeter;

public class MeterTestApp extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		BarMeter meter = new BarMeter();
		meter.setPrefSize(256, 24);
		meter.setOrientation(Orientation.HORIZONTAL);
		
		TouchFader fader = new TouchFader();
		fader.minProperty().bind(meter.minProperty());
		fader.maxProperty().bind(meter.maxProperty());
		fader.setOrientation(Orientation.HORIZONTAL);
		
		meter.valueProperty().bind(fader.valueProperty());
		
		VBox container = new VBox(meter, fader);
		container.setSpacing(64);
		
		container.setPadding(new Insets(32));
		primaryStage.setScene(new Scene(container));
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
