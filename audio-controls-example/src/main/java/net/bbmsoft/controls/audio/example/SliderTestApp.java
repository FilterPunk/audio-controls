package net.bbmsoft.controls.audio.example;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import net.bbmsoft.controls.audio.slider.PotiSlider;

public class SliderTestApp extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		PotiSlider slider1 = new PotiSlider();
		PotiSlider slider2 = new PotiSlider();
		PotiSlider slider3 = new PotiSlider();
		
		slider1.setMaxWidth(Double.MAX_VALUE);
		slider1.setMaxHeight(Double.MAX_VALUE);
		slider1.setPrefWidth(64);
		slider1.setPrefHeight(64);

		slider2.setMaxWidth(Double.MAX_VALUE);
		slider2.setMaxHeight(Double.MAX_VALUE);
		slider2.setPrefWidth(64);
		slider2.setPrefHeight(64);

		slider3.setMaxWidth(Double.MAX_VALUE);
		slider3.setMaxHeight(Double.MAX_VALUE);
		slider3.setPrefWidth(64);
		slider3.setPrefHeight(64);
		
		
		HBox root = new HBox(slider1, slider2, slider3);
		root.setSpacing(8);
		root.setPadding(new Insets(8));
		
		primaryStage.setScene(new Scene(root));
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}
}
