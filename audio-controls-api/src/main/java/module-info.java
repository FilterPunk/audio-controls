module net.bbmsoft.audio.controls.api {
    requires transitive javafx.controls;
    requires transitive javafx.graphics;
    exports net.bbmsoft.controls.audio;
}