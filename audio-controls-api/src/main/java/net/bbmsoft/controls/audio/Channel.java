package net.bbmsoft.controls.audio;

public interface Channel {

	public String getLabel();

	public interface Input extends Channel {
	}

	public interface Output extends Channel {
	}
}
