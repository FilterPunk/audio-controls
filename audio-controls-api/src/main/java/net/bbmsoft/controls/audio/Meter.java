package net.bbmsoft.controls.audio;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;

public interface Meter extends SingleAxisWidget {

	public DoubleProperty peakValueProperty();

	public double getPeakValue();

	public void setPeakValue(double value);

	public ReadOnlyBooleanProperty clippedProperty();

	public boolean isClipped();

	public DoubleProperty fallbackProperty();

	public double getFallback();

	public void setFallback(double value);

	public DoubleProperty peakFallbackProperty();

	public double getPeakFallback();

	public void setPeakFallback(double value);

	public DoubleProperty holdProperty();

	public double getHold();

	public void setHold(double value);

	public DoubleProperty peakHoldProperty();

	public double getPeakHold();

	public void setPeakHold(double value);

	public BooleanProperty autoFallbackProperty();

	public boolean isAutoFallback();

	public void setAutoFallback(boolean value);

}
