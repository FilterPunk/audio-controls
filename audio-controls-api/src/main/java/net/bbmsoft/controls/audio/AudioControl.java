package net.bbmsoft.controls.audio;

import javafx.scene.control.Control;

public interface AudioControl extends Touchable<Control> {

	public default Control getControl() {
		if (this instanceof Control) {
			return (Control) this;
		} else {
			throw new IllegalStateException(
					"AudioControl that are not derived from javafx.scene.control.Control must override the getControl() method!");
		}
	}
}
