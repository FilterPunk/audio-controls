package net.bbmsoft.controls.audio;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;

public interface EQ {

	public interface Band {

		public enum Type {
			BELL, HIGH_PASS, LOW_PASS, HIGH_SHELF, LOW_SHELF, NOTCH, BAND_PASS;
		}

		public DoubleProperty frequencyProperty();

		public double getFrequency();

		public void setFrequency(double value);

		public DoubleProperty gainProperty();

		public double getGain();

		public void setGain(double value);

		public DoubleProperty qProperty();

		public double getQ();

		public void setQ(double value);

		public ObjectProperty<Type> bandTypeProperty();

		public Type getBandType();

		public void setBandType(Type value);

		public BooleanProperty activeProperty();

		public boolean isActive();

		public void setActive(boolean value);

		public IntegerProperty slopeProperty();

		public int getSlope();

		public void setSlope(int value);
	}

	public IntegerProperty bandCountProperty();

	public void setBandCount(int value);

	public int getBandCount();

	public ObservableList<Band> getBands();

	public DoubleProperty minGainProperty();

	public double getMinGain();

	public void setMinGain(double value);

	public DoubleProperty maxGainProperty();

	public double getMaxGain();

	public void setMaxGain(double value);

	public DoubleProperty minFrequencyProperty();

	public double getMinFrequency();

	public void setMinFrequency(double value);

	public DoubleProperty maxFrequencyProperty();

	public double getMaxFrequency();

	public void setMaxFrequency(double value);

	public DoubleProperty minQProperty();

	public double getMinQ();

	public void setMinQ(double value);

	public DoubleProperty maxQProperty();

	public double getMaxQ();

	public void setMaxQ(double value);

	public ObjectProperty<Curve> frequencyCurveProperty();

	public Curve getFrequencyCurve();

	public void setFrequencyCurve(Curve value);

	public ObjectProperty<Curve> gainCurveProperty();

	public Curve getGainCurve();

	public void setGainCurve(Curve value);
	
	public BooleanProperty activeProperty();
	
	public boolean isActive();
	
	public void setActive(boolean value);

	public void reset();
}
